package com.socks.tests.brutal;

import com.socks.api.dataOfUsers.DataAllUsers;
import com.socks.api.models.UserForAPI;
import com.socks.api.services.UserApiService;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static com.socks.api.conditions.Conditions.*;
import static org.hamcrest.Matchers.*;

public class UserForAPIManagerTests {

    private UserApiService userApiService = new UserApiService();

    @BeforeAll
    static void setUp() {
        RestAssured.baseURI = "http://127.0.0.1/";
    }

    //////////
//    Registrations new user on the page => /register
    //////////
    @Test
    public void testCanRegisterAsAllValidUser() {
        //      given
        UserForAPI userForAPI = DataAllUsers.getProviderNumOne().getUserRegistrationWithAllValidFields();
        //      expect
        userApiService.registerUser(userForAPI)
                .shouldHave(body("id", not(isEmptyString())))
                .shouldHave(statusCode(200));
    }

    @Test
    public void testCanNotRegisterWithOutFieldUsername() {
//          given
        UserForAPI userForAPI = DataAllUsers.getProviderNumOne().getUserRegistrationWithOutFieladUsername();
//          expect
        userApiService.registerUser(userForAPI)
                .shouldHave(statusCode(500))
                .shouldHave(header("Content-Length", is("0")));
    }

    @Test
    public void testCanRegisterWithFieldUsername() {
//          given
        UserForAPI userForAPI = DataAllUsers.getProviderNumOne().getUserRegistrationWithFieldUsername();
//          expect
        userApiService.registerUser(userForAPI)
                .shouldHave(body("id", not(isEmptyString())))
                .shouldHave(statusCode(200));
    }

    @Test
    public void testCanRegisterEmptyFieldFirstName() {
//          given
        UserForAPI userForAPI = DataAllUsers.getProviderNumOne().getUserRegistrationWithoutFieldFirstName();
//          expect
        userApiService.registerUser(userForAPI)
                .shouldHave(body("id", not(isEmptyString())))
                .shouldHave(statusCode(200));
    }

    @Test
    public void testCanRegisterEmptyLastName() {
        //          given
        UserForAPI userForAPI = DataAllUsers.getProviderNumOne().getUserRegistrationWithoutFieldLastName();
        //          expect
        userApiService.registerUser(userForAPI)
                .shouldHave(body("id", not(isEmptyString())))
                .shouldHave(statusCode(200));
    }

    @Test
    public void testCanRegisterEmptyEmail() {
        //          given
        UserForAPI userForAPI = DataAllUsers.getProviderNumOne().getUserRegistrationWithoutFieldEmail();
        //          expect
        userApiService.registerUser(userForAPI)
                .shouldHave(body("id", not(isEmptyString())))
                .shouldHave(statusCode(200));
    }

    @Test
    public void testCanRegisterEmptyPassword() {
        //          given
        UserForAPI userForAPI = DataAllUsers.getProviderNumOne().getUserRegistrationWithoutFieldPassword();
        //          expect
        userApiService.registerUser(userForAPI)
                .shouldHave(body("id", not(isEmptyString())))
                .shouldHave(statusCode(200));
    }

    ////////////
//    Login user on the page => /login
    ///////////
    @Test
    public void testCanLoginWithValidCredentials() {
//              given
        UserForAPI userForAPI = DataAllUsers.getProviderNumOne().getLoginUser_200();
//              expect
        userApiService.login(userForAPI)
                .shouldHave(statusCode(200));
    }

    @Test
    public void testCanNotLoginWithInvalidCredentials() {
//              given
        UserForAPI userForAPI = DataAllUsers.getProviderNumOne().getUserRegistrationWithAllValidFields();
//              expect
        userApiService.login(userForAPI)
                .shouldHave(statusCode(401))
                .shouldHave(statusLine("HTTP/1.1 401 Unauthorized"));
    }

}



