package com.socks.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.socks.api.models.ProviderNumTwo;
import com.socks.api.models.UserForAPI;
import com.socks.api.utilize.StringUtilize;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;

@Slf4j
public class HelpDateFile {
    private ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
    private String basePath = "src/main/resources/";
    private String yamlFileName = "validAPIDateForAllUsers.yaml";

    public UserForAPI buildUser() {
        try {
            UserForAPI userForAPI = mapper.readValue(new File(basePath + yamlFileName), UserForAPI.class);
            return userForAPI
                    .setUsername(StringUtilize.generateRandomName());
//                    .setPassword((StringUtilize.generateRandomName()));
        } catch (IOException e) {
            log.error(e.toString());
        }
        return null;
    }

    public ProviderNumTwo buildProviderNumTwo() {
        try {
            return mapper.readValue(new File(basePath + yamlFileName), ProviderNumTwo.class);
        } catch (IOException e) {
            log.error(e.toString());
        }
        return null;
    }

}
