package com.socks.api;

public final class PagesSite {
    public static final String registerNewUserPage = "/register";
    public static final String loginUserPage = "/login";
    public static final String getCustomersPage = "/customers";

}
