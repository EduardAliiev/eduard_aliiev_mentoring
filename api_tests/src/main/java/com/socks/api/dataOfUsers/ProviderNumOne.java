package com.socks.api.dataOfUsers;

import com.socks.api.HelpDateFile;
import com.socks.api.models.UserForAPI;

public class ProviderNumOne {
    HelpDateFile helpDateFile = new HelpDateFile();

    public UserForAPI getUserRegistrationWithAllValidFields() {
        return helpDateFile.buildUser();
    }

    public UserForAPI getUserRegistrationWithFieldUsername() {
        return helpDateFile.buildUser()
                .setFirstName(null)
                .setLastName(null)
                .setEmail(null)
                .setPassword(null);
    }

    public UserForAPI getUserRegistrationWithOutFieladUsername() {
        return helpDateFile.buildUser()
                .setUsername(null);
    }

    public UserForAPI getUserRegistrationWithoutFieldFirstName() {
        return helpDateFile.buildUser()
                .setFirstName(null);
    }

    public UserForAPI getUserRegistrationWithoutFieldLastName() {
        return helpDateFile.buildUser()
                .setLastName(null);
    }

    public UserForAPI getUserRegistrationWithoutFieldEmail() {
        return helpDateFile.buildUser()
                .setEmail(null);
    }

    public UserForAPI getUserRegistrationWithoutFieldPassword() {
        return helpDateFile.buildUser()
                .setPassword(null);
    }

    public UserForAPI getLoginUser_200() {
        return helpDateFile.buildUser()
                .setUsername("user")
                .setPassword("password");
    }

}

