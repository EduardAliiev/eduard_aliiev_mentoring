package com.socks.api.services;

import com.socks.api.AssertableResponse;
import com.socks.api.PagesSite;
import com.socks.api.models.UserForAPI;
import io.qameta.allure.Step;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserApiService {

    public RequestSpecification setUpchik() {
        return RestAssured.given()
                .contentType(ContentType.JSON)
                .filters(new RequestLoggingFilter(),
                        new ResponseLoggingFilter(),
                        new AllureRestAssured());
    }

    @Step
    public AssertableResponse registerUser(UserForAPI userForAPI) {
        log.info("Register UserForAPI {}", userForAPI);
        return new AssertableResponse(setUpchik()
                .body(userForAPI)
                .when()
                .post("Register")
                .then());

    }

    @Step
    public AssertableResponse login(UserForAPI userForAPI) {
        String userName = userForAPI.getUsername();
        String password = userForAPI.getPassword();
        log.info("Logging with credentials: {} / {}", userName, password);
        return new AssertableResponse(setUpchik()
                .auth()
                .preemptive()
                .basic(userName, password)
                .when()
                .get(PagesSite.loginUserPage)
                .then());
    }
}

