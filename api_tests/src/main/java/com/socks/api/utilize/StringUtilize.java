package com.socks.api.utilize;

import org.apache.commons.lang3.RandomStringUtils;

public class StringUtilize {
    public static String generateRandomName() {
        int numOfCharsInName = 6;
        return RandomStringUtils.randomAlphanumeric(numOfCharsInName);
    }
}
