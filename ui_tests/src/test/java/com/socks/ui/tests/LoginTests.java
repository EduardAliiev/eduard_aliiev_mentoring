package com.socks.ui.tests;

import com.socks.api.dataOfUsers.DataAllUsers;
import com.socks.api.models.UserForAPI;
import com.socks.ui.pages.MainPage;
import com.socks.ui.pages.modals.LoginModal;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.exactText;

public class LoginTests extends BaseTest {

    @Test
    public void testCanLoginWithValidCredentials() {
        //      given
        UserForAPI userForAPI = DataAllUsers.getProviderNumOne().getLoginUser_200();
        //      expect
        MainPage.visit()
                .openLoginFormBtn();

        at(LoginModal.class)
                .submitFromFor(userForAPI);

        at(MainPage.class).loggedUserLabel.shouldHave(exactText("Logged in as User Name"));
    }

    @Test
    public void testCannotLoginWithInvalidPassword() {
        //      given
        UserForAPI userForAPI = DataAllUsers.getProviderNumOne().getLoginUser_200();
        userForAPI.setPassword("invalid");
        //      expect
        MainPage.visit()
                .openLoginFormBtn();

        at(LoginModal.class)
                .submitFromFor(userForAPI);

        at(LoginModal.class).invalidLoginAlert.shouldHave(exactText("Invalid login credentials."));
    }

}
