package com.socks.ui.tests;

import com.socks.ui.pages.MainPage;
import com.socks.ui.pages.ProductsPage;
import com.socks.ui.pages.widgets.FiltersDisplayWidget;
import com.socks.ui.pages.widgets.FiltersWidget;
import org.testng.annotations.Test;

import static com.codeborne.selenide.CollectionCondition.size;

public class FilteringTests extends BaseTest {

    @Test
    public void testCanFilterBySingleCategorys() {
        //      expect
        MainPage.visit()
                .openCatalogueBtn();

        at(ProductsPage.class).products.shouldHave(size(6));

        at(FiltersWidget.class).
                filterBy("geek");

        at(ProductsPage.class).products.shouldHave(size(1));
    }

    @Test
    public void testCanFilterByMultipleCategories() {
        //      expect
        MainPage.visit()
                .openCatalogueBtn();

        at(FiltersWidget.class).
                filterBy("geek", "action", "black", "green");

        at(ProductsPage.class).products.shouldHave(size(6));
    }

    @Test
    public void testCanClearFilters() {
        //      expect
        MainPage.visit()
                .openCatalogueBtn();

        at(ProductsPage.class).products.shouldHave(size(6));

        at(FiltersWidget.class).
                filterBy("geek");

        at(ProductsPage.class).products.shouldHave(size(1));

        at(FiltersWidget.class).clearFiltersBtn();

        at(ProductsPage.class).products.shouldHave(size(6));
    }

    @Test
    public void testCanFilterToShowLessProducts() {
        //      expect
        MainPage.visit()
                .openCatalogueBtn();

        at(ProductsPage.class).products.shouldHave(size(6));

        at(FiltersDisplayWidget.class).filterByPaginator(3);

        at(ProductsPage.class).products.shouldHave(size(3));
    }

    @Test
    public void testCanFilterToShowMoreProducts() {
        //      expect
        MainPage.visit()
                .openCatalogueBtn();

        at(ProductsPage.class).products.shouldHave(size(6));

        at(FiltersDisplayWidget.class).filterByPaginator(9);

        at(ProductsPage.class).products.shouldHave(size(9));
    }


}
