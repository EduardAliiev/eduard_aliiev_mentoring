package com.socks.ui.tests;

import com.socks.api.dataOfUsers.DataAllUsers;
import com.socks.api.models.UserForAPI;
import com.socks.ui.pages.MainPage;
import com.socks.ui.pages.modals.RegisterModal;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.exactText;

public class RegistrationTests extends BaseTest {

    @Test
    public void testCanRegisterNewUser() {
        //      given
        UserForAPI userForAPI = DataAllUsers.getProviderNumOne().getUserRegistrationWithAllValidFields();
        String loginExpectedText = String.format("Logged in as %s %s", userForAPI.getFirstName(), userForAPI.getLastName());
        //      expect
        MainPage.visit()
                .openRegisterFormBtn();

        at(RegisterModal.class)
                .submitFromFor(userForAPI);

        at(MainPage.class).loggedUserLabel.shouldHave(exactText(loginExpectedText));
    }

}
