package com.socks.ui.pages.modals;

import com.codeborne.selenide.SelenideElement;
import com.socks.api.models.UserForAPI;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class RegisterModal {

    private SelenideElement userName = $(By.xpath("//*[@id='register-username-modal']"));
    private SelenideElement firstName = $(By.xpath("//*[@id='register-first-modal']"));
    private SelenideElement lastName = $(By.xpath("//*[@id='register-last-modal']"));
    private SelenideElement email = $(By.xpath("//*[@id='register-email-modal']"));
    private SelenideElement password = $(By.xpath("//*[@id='register-password-modal']"));
    private SelenideElement registerBtn = $(By.xpath("(//*[@id='register-modal']//button)[2]"));

    public void submitFromFor(UserForAPI userForAPI) {
        userName.setValue(userForAPI.getUsername());
        firstName.setValue(userForAPI.getFirstName());
        lastName.setValue(userForAPI.getLastName());
        email.setValue(userForAPI.getEmail());
        password.setValue(userForAPI.getPassword());
        registerBtn.click();
    }

}
