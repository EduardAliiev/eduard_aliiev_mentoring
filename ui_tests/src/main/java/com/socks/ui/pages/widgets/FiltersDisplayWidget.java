package com.socks.ui.pages.widgets;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$$;

public class FiltersDisplayWidget {
//    Display three paginator pages
    public void filterByPaginator(int numOfProductsToDisplay) {
        $$(By.xpath("//*[@id='products-number']//a")).filterBy(text(String.valueOf(numOfProductsToDisplay)))
                .first().click();
    }

}
