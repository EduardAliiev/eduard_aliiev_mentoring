package com.socks.ui.pages.widgets;

import org.openqa.selenium.By;

import java.util.Arrays;

import static com.codeborne.selenide.Selenide.$;

public class FiltersWidget {

//    Select filter by color and [Apply] button
    public void filterBy(String... categories) {
        Arrays.stream(categories).forEach(c -> $(String.format("[value=%s]", c)).click());
        $(By.xpath("//*[@id='filters-form']/a")).click();
    }

//    [Clear] button
    public void clearFiltersBtn() {
        $(By.xpath("//*[@onclick='resetTags()']")).click();
    }

}
