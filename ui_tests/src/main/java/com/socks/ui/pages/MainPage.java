package com.socks.ui.pages;

import com.codeborne.selenide.SelenideElement;
import com.socks.ui.pages.modals.LoginModal;
import com.socks.ui.pages.modals.RegisterModal;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class MainPage {

    public static MainPage visit() {
        return open("/", MainPage.class);
    }

    //  Header of general page
    //  Authorized user field "Logged in as ......"
    public SelenideElement loggedUserLabel = $(By.xpath("//*[@id='howdy']/a"));

    //  [Register] button
    public RegisterModal openRegisterFormBtn() {
        $(By.xpath("//*[@id='register']/a")).click();
        return new RegisterModal();
    }

    //  [Login] button
    public LoginModal openLoginFormBtn() {
        $(By.xpath("//*[@id='login']/a")).click();
        return new LoginModal();
    }

    //  [Logout] button
    public LoginModal openLogoutBtn() {
        $(By.xpath("//*[@id='logout']/a")).click();
        return new LoginModal();
    }

    //  Navigation block
    //  [HOME] button
    public void refreshHomeBtn() {
        $(By.xpath("//*[@id='tabIndex']/a")).click();
    }

    //  [CATALOGUE] button
    public void openCatalogueBtn() {
        $(By.xpath("//*[@id='tabCatalogue']//b")).click();
    }

    //  [ACCOUNT] button
    public void openAccountBtn() {
        $(By.xpath("//*[@id='tabAccount']/a")).click();
    }


}

