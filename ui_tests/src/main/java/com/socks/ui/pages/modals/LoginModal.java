package com.socks.ui.pages.modals;

import com.codeborne.selenide.SelenideElement;
import com.socks.api.models.UserForAPI;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class LoginModal {

    public SelenideElement invalidLoginAlert = $(By.xpath("//*[@id='login-message']/div"));

    public void submitFromFor(UserForAPI userForAPI) {
        $(By.xpath("//*[@id='username-modal']")).setValue(userForAPI.getUsername());
        $(By.xpath("//*[@id='password-modal']")).setValue(userForAPI.getPassword());
        $(By.xpath("(//*[@id='login-modal']//button)[2]")).click();
    }

}
